# How-to
* Development: `npm start`.
* Build: `npm run build`.

# Learnings
* Locations of files are no longer specified in a config file as is the case with Gulp. Only "entry point files" are specified. These are JavaScript files and are parsed by webpack to build a "dependency graph". Webpack only “knows” about files in your project which it encounters in this graph.

* Every filetypes other than JavaScript, such as stylesheets and images, need to be imported in JavaScript. This is done the same way as importing modules, and works by implementing `loaders`. To me, this makes webpack more suitable as a build tool for frameworks such as React and Vue where components are rendered via JavaScript too.

* There are plugins and built-in functionalities for a local development server, live reloading, minifying, etc.

* Webpack offers [Hot Module Replacement](https://webpack.js.org/guides/hot-module-replacement/) to add/update/remove modules while an application is running without a full reload. However this requires quite some [configuration](https://webpack.js.org/api/hot-module-replacement/) across code where you want to use it.

* Webpack offers [tree shaking](https://webpack.js.org/guides/tree-shaking/) to remove "dead" code when minifying. To make it work with Babel, turn conversion to CommonJS modules off.

* Webpack offers [code splitting](https://webpack.js.org/guides/code-splitting/) to automatically move duplicate modules to a separate bundle. This does however create a **lot** of boilerplate code in that separate bundle.

* CSS can be extracted from the dependency graph with [ExtractTextWebpackPlugin](https://webpack.js.org/plugins/extract-text-webpack-plugin/), to generate a separate stylesheet.

* HTML files can be generated with [HtmlWebpackPlugin](https://webpack.js.org/plugins/html-webpack-plugin/), where paths to JavaScript and CSS are set correctly automatically.
