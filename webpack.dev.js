const merge = require('webpack-merge');
const common = require('./webpack.common.js');

module.exports = merge(common, {
	// this option controls if and how source maps are generated
	devtool: 'inline-source-map',

	// provides a simple web server and the ability to use live reloading
	devServer: {
		contentBase: './dist'
	}
});
