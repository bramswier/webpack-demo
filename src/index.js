import create from './components/element/element';
import { cube } from './math.js';

create();

console.log(`5 cubed is equal to ${cube(5)}`);
