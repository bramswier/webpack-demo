import './element.css';
import interiorRoomSofa from './interior-room-sofa.jpg';
import printMe from './print.js';

const create = () => {
	const element = document.createElement('section');
	element.classList.add('element');

	const paragraph = document.createElement('p');
	paragraph.innerHTML = 'Made using webpack';
	element.appendChild(paragraph);

	const image = new Image();
	image.src = interiorRoomSofa;
	element.appendChild(image);

	const button = document.createElement('button');
	button.innerHTML = 'Click me and check the console';
	button.onclick = printMe;
	element.appendChild(button);

	document.body.appendChild(element);
}

export default create;
