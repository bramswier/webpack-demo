const path = require('path'); // built in in node.js
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const paths = {
	src: path.join(__dirname, '/src'),
	dist: path.join(__dirname, '/dist')
}

module.exports = {
	entry: {
		app: `${paths.src}/index.js`,
		another: `${paths.src}/another-module.js`,
	},
	plugins: [
		new CleanWebpackPlugin(['dist']),

		// generates an index.html file with the correct references to bundle names
		new HtmlWebpackPlugin({
			title: 'Webpack Demo'
		}),

		// allows to extract common dependencies into an existing entry chunk or an entirely new chunk
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common'
		}),

		// extract text (in this case css) from a bundle, or bundles, into a separate file
		new ExtractTextPlugin('style.css')
	],
	output: {
		path: paths.dist,
		filename: '[name].[chunkhash].js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				include: paths.src,
				use: {
		    		loader: 'babel-loader',
		    		options: {
		      			presets: [
							['env', {
								'modules': false // disable transformation to commonjs, to enable webpack tree-shaking (see comments in http://2ality.com/2015/12/webpack-tree-shaking.html)
							}]
						]
		    		}
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					fallback: "style-loader", // adds CSS to the DOM with inline styling
					use: "css-loader" // allows loading of CSS in JavaScript
				})
			},
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [
					// instructs webpack to emit the required object as file and to return its public URL
					'file-loader'
				]
			},
			{
				test: /\.(woff|woff2|eot|ttf|otf)$/,
				use: [ 'file-loader' ]
			}
		]
	}
}
